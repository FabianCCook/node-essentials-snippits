var Domain = require( 'domain' ),
	domain;

domain = Domain.create( );

domain.on( 'error', function( error ) {
	console.log( 'Domain error', error.message );
});

process.nextTick( function( ) {
	domain.run( function( ) {
		throw new Error( 'Error happened' );
	});
	console.log( "I won't execute" );
}); 

process.nextTick( function( ) {
	console.log( 'Next tick happend!' );
});

console.log( 'I happened before everything else' );