var Bunyan = require( 'bunyan' ),
	logger = Bunyan.createLogger( {
		name: 'example-13'
	}),
	Q = require( 'q' );
	
Q( )
.then( function ourOwnCallback() {
	// Promised returned from another function
	return Q( )
	.then( function resultFromOtherFunction( ) {
		throw new Error( 'Hello errors' ); 
	});
})
.fail( function( error ) {
	logger.error( error );
});