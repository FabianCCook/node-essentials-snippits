var Http = require( 'http' ); 

var count = 0;

var server = Http.createServer( function ( request, response ) {
	var message,
		status = 200;

	count += 1;


	switch( request.url ) {
		case '/count':
			message = count.toString( );
			break;
		case '/hello':
			message = 'World';
			break;
		default: 
			status = 404;
			message = 'Not Found';
			break;
	}

	response.writeHead(status, {'Content-Type': 'text/plain'});
	console.log( request.method, request.url, status, message );
	response.end( message );
});

server.listen( 8000, function( ) {
	console.log( 'Listening on port 8000' ); 
} );