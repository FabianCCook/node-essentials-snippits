var Http = require( 'http' );
	
Http.createServer( function( request, response ) {
	console.log( 
		'Received request', 
		request.method,
		request.url 
	)
	
	console.log( 'Returning 200' );
	
	response.writeHead( 200, { 'Content-Type': 'text/plain' } );
	response.end( 'Hello World\n' );

}).listen( 8000 );

console.log( 'Server running on port 8000' ); 
