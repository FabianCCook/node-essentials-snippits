var MongoDB = require('mongodb'),
	MongoClient = MongoDB.MongoClient,
	ObjectId = MongoDB.ObjectId;

connection = "mongodb://localhost:27017/nodejs-essentials"

MongoClient.connect( connection, function( error, db ) {
    if( error ) return console.log( error );

	console.log( 'We have a connection!' );

	var collection = db.collection( 'collection_name' );

	var doc = { 
		key: 'value'	
	};

	collection.save( doc, { w: 1 }, function( ) {
		console.log( 'Document saved' );
	});
 
	// This document already exists in my database
	var doc_id = {
		_id: new ObjectId( "55b4b1ffa31f48c6fa33a62a" ),
		key: 'value'
	};
	collection.save( doc_id, { w: 1 }, function( ) {
		console.log( 'Document with ID saved' );
	});

});