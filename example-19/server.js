var users = {
	foo: {
		username: 'foo',
		password: 'bar',
		id: 1
	},
	bar: {
		username: 'bar',
		password: 'foo',
		id: 2
	}
}

var Passport = require( 'passport' ),
	LocalStrategy = require( 'passport-local' ).Strategy;
	
var localStrategy = new LocalStrategy({
    usernameField: 'username',
    passwordField: 'password'
  },
  function(username, password, done) {
    user = users[ username ];

	if ( user == null ) {
		return done( null, false, { message: 'Invalid user' } ); 
	}
	
	if ( user.password !== password ) {
		return done( null, false, { message: 'Invalid password' } );	
	}

	done( null, user );
  }
)

Passport.use( 'local', localStrategy );

var Express = require( 'express' );
	
var app = Express( );

var BodyParser = require( 'body-parser' );

app.use( BodyParser.urlencoded( { extended: false } ) );
app.use( BodyParser.json( ) );
app.use( Passport.initialize( ) );

var JSONWebToken = require( 'jsonwebtoken' ),
	Crypto = require( 'crypto' );

app.post( 
	'/login',
	Passport.authenticate( 'local', { session: false } ),
	function ( request, response ) {
		var user = request.user;	
		// The payload just contains the id of the user
		// and their username, we can verify whether the claim
		// is correct using JSONWebToken.verify		
		var payload = {
			id: user.id,
			username: user.username	
		};
		// Generate a random string
		// Usually this would be an app wide constant
		// But can be done both ways
		var secret = Crypto.randomBytes( 128 )
						   .toString( 'base64' );
	    // Create the token with a payload and secret
		var token = JSONWebToken.sign( payload, secret );
		
		// The user is still referencing the same object
		// in users, so no need to set it again
		request.user.secret = secret

		// Return the user a token to use
		response.send( token );
	}
);

var BearerStrategy = require( 'passport-http-bearer' ).Strategy;
		
var bearerStrategy = new BearerStrategy( 
	function( token, done ) {
		var payload = JSONWebToken.decode( token );
		var user = users[ payload.username ];
		// If we can't find a user, or the information 
		// doesn't match then return false
		if ( user == null ||
		     user.id !== payload.id || 
		     user.username !== payload.username ) {
			return done( null, false );
		}
		// Ensure the token is valid now we have a user
		JSONWebToken.verify( token, user.secret, function ( error, decoded ) {
			if ( error || decoded == null ) {
				return done( error, false );
			}
			return done( null, user );
		});
	}
)

Passport.use( 'bearer', bearerStrategy);

app.get( 
	'/userinfo',
	Passport.authenticate( 'bearer', { session: false } ),
	function ( request, response ) {
		var user = request.user;
		response.send( {
			id: user.id,
			username: user.username
		});
	} 
);


app.listen( 8080, function( ) {
	console.log( 'Listening on port 8080' );
});
