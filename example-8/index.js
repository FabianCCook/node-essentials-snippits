var Bunyan = require( 'bunyan' ),
	logger;

logger = Bunyan.createLogger( {
	name: 'example-8',
	level: Bunyan.TRACE
});

logger.trace( 'Trace' );
logger.debug( 'Debug' );
logger.info( 'Info' );
logger.warn( 'Warn' );
logger.error( 'Error' );
logger.fatal( 'Fatal' );

logger.fatal( 'We got a fatal, lets exit' );
process.exit( 1 );