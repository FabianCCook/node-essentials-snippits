var Bunyan = require( 'bunyan' ),
	logger = Bunyan.createLogger( {
		name: 'example-12'
	});

try {
	a = function( callback ) {
		return function( ) {
			callback( );
		};
	};
	b = function( callback ) {
		return function( ) {
			callback( );
		}
	};
	c = function( callback ) {
		return function( ) {
			throw new Error( "I'm just messing with you" ); 
		};
	};
	a( b( c( ) ) )( );
} catch ( error ) {
	logger.error( error );
}

function Cls() {

}

Cls.prototype.func = function( ) {
	throw new Error( 'Test' );
}

try {
	( new Cls( ) ).func( )
} catch ( error ) {
	logger.error( error );
}


function func( ) {
	throw new Error( 'Test' );
}

try {
	func( );
} catch ( error ) {
	logger.error( error );
}