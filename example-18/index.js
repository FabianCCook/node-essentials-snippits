var LevelUP = require( 'level' ),
	db = new LevelUP( './example-db', {
		valueEncoding: 'json'
	});

db.put( 'key', 'value', function ( error ) {
	if ( error ) return console.log( 'Error!', error )

	db.get( 'key', function( error, value ) {
		if ( error ) return console.log( 'Error!', error )

		console.log( "key =", value )
	});
});

db.put( 'jsonKey', { inner: 'value' }, function ( error ) {
	if ( error ) return console.log( 'Error!', error )

	db.get( 'jsonKey', function( error, value ) {
		if ( error ) return console.log( 'Error!', error )

		console.log( "jsonKey =", value )
	});
});

