var Domain = require( 'domain' ),
	domain;

domain = Domain.create( );

domain.on( 'error', function( error ) {
	console.log( 'Domain error', error.message );
});

domain.run( function( ) {
	// Run code inside domain
	console.log( process.domain === domain );
	throw new Error( 'Error happened' ); 
});